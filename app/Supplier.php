<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supplier extends Model
{

    use SoftDeletes;
    //
    protected $table = 'suppliers';

    protected $fillable = ['nome', 'site', 'uf', 'cidade', 'email'];

    public function products()
    {
        return $this->hasMany('App\Product', 'supplier_id', 'id');
    }
}
