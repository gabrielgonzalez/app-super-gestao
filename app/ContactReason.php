<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactReason extends Model
{
    protected $table = 'contact_reasons';

    protected $fillable = ['description', 'alias'];
}
