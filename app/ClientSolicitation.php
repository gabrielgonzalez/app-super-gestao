<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientSolicitation extends Model
{
    //
    protected $table = 'client_solicitations';

    protected $fillable = ['solicitation_id', 'product_id', 'quantity'];

    public function solicitations()
    {
        return $this->belongsTo('App\Solicitation', 'solicitation_id', 'id');
    }

    public function products()
    {
        return $this->belongsTo('App\Product', 'product_id', 'id');
    }
}
