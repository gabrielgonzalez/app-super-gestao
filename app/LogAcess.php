<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogAcess extends Model
{
    protected $table = 'log_acesses';

    protected $fillable = ['category', 'data'];
}
