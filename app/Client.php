<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'clients';

    protected $fillable = ['name', 'email', 'cpfCnpj', 'zipCode'];

    public function solicitations()
    {
        return $this->belongsTo('App\Solicitation');
    }
}
