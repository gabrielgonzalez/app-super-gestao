<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteContact extends Model
{
    //
    protected $table = 'site_contacts';

    protected $fillable = ['name', 'phone', 'message', 'email', 'contact_reasons_id'];
}
