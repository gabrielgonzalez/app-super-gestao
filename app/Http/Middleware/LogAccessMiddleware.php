<?php

namespace App\Http\Middleware;

use App\LogAcess;
use Closure;

class LogAccessMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $category = $request->method() . " | " . $request->getPathInfo();
        $dataRequest = [
            "IP" => $request->server->get('REMOTE_ADDR'),
            "content" => $request->getContent(),
            "query" => $request->query(),
        ];

        // $response = $next($request);
        // $dataRequest['response'] = $response;
        // return $response;

        LogAcess::create([
            'category' => $category,
            'data' => json_encode($dataRequest),
        ]);

        return $next($request);
    }
}
