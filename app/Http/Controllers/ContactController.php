<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SiteContact;
use App\ContactReason;

class ContactController extends Controller
{

    public function index()
    {

        $contact_categories = ContactReason::all();

        return view('site.contact', ['contact_categories' => $contact_categories]);
    }

    public function sendMessage(Request $request)
    {

        $request->validate(
            [
                'name' => 'required|min:3|max:150',
                'phone' => 'required',
                'email' => 'email',
                'contact_reasons_id' => 'required',
                'message' => 'required|max:2000'
            ],
            [
                'email.email' => 'O campo E-mail deve possuir um e-mail válido',
                'contact_reasons_id.required' => 'Você precisa escolher o motivo do contato!',
                'message.max' => 'O campo mensagem não pode ultrapassar 2000 caracteres!',

                'required' => 'O campo :attribute deve ser preenchido!',
            ]
        );
        // METODO PARA SALVAR INFORMANDO OS CAMPOS
        // $contact = new SiteContact();

        // $contact->name = $request->input('name');
        // $contact->phone = $request->input('fone');
        // $contact->message = $request->input('message');
        // $contact->email = $request->input('email');
        // $contact->contact_category = $request->input('category');

        // if ($contact->save()) {
        //     return redirect()->action('ContactController@index');
        // }

        // ATALHO DO METODO SAVE
        // SiteContact::create($request->all());

        // METODO PARA SALVAR AS INFORMAÇÕES USANDO O FILL
        $contact = new SiteContact();
        $contact->fill($request->all());

        if ($contact->save()) {
            return redirect()->route('site.index');
        }
    }
}
