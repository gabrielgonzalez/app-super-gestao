<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!empty($request->all())) {

            $clientsList = Client::with(['solicitations'])
                ->where('name', 'like', "%" . $request->input('name')."%")
                ->where('email', 'like', "%" . $request->input('email') . "%")
                ->where('cpfCnpj', 'like', "%" . $request->input('cpfCnpj') . "%")
                ->where('zipCode', 'like', "%" . $request->input('zipCode') . "%")
                ->paginate(20);

        } else {
            $clientsList = Client::with(['solicitations'])->paginate(20);
        }

        return view('app.client.index', ['clientsList' => $clientsList, 'queryParams' => $request->all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('app.client.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate(
            [
                'name' => 'required|min:3|max:150',
                'email' => 'required|email',
                'cpfCnpj' => 'required',
                'zipCode' => 'required',
            ],
            [
                'required' => 'O campo deve ser preenchido!',
                'min' => 'O campo deve conter no mínimo 3 caracteres!',
                'max' => 'Limite de caracteres atingido!'
            ]
        );

        $client = new Client();
        $client->fill($request->all());

        if ($client->save()) {
            return redirect()->route('clients.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $client = Client::find($id);

        return view('app.client.show', ['client' => $client]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $client = Client::find($id);

        return view('app.client.edit', ['client' => $client]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate(
            [
                'name' => 'required|min:3|max:150',
                'email' => 'required|email',
                'cpfCnpj' => 'required',
                'zipCode' => 'required',
            ],
            [
                'required' => 'O campo deve ser preenchido!',
                'min' => 'O campo deve conter no mínimo 3 caracteres!',
                'max' => 'Limite de caracteres atingido!'
            ]
        );

        $client = Client::find($id);

        if ($client->update($request->all())) {
            return redirect()->route('clients.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $client = Client::find($id);

        if ($client->delete()) {

            return redirect()->route('clients.index');

        }
    }
}
