<?php

namespace App\Http\Controllers;

use App\Client;
use App\ClientSolicitation;
use App\Product;
use App\Solicitation;
use Illuminate\Http\Request;

class ClientSolicitationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!empty($request->all())) {

            $clientsList = Client::with(['solicitations'])
                ->where('name', 'like', "%" . $request->input('name')."%")
                ->where('email', 'like', "%" . $request->input('email') . "%")
                ->where('cpfCnpj', 'like', "%" . $request->input('cpfCnpj') . "%")
                ->where('zipCode', 'like', "%" . $request->input('zipCode') . "%")
                ->paginate(20);

        } else {
            $clientsList = Client::with(['solicitations'])->paginate(20);
        }

        return view('app.clientsSolicitations.index', []);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $solicitation = Solicitation::find($request->query('solicitation'));

        $productList = Product::all();

        return view('app.clientsSolicitations.create', ['solicitation' => $solicitation, 'productList' => $productList]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate(
            [
                'solicitation_id' => 'exists:solicitations,id',
                'product_id' => 'exists:products,id',
                'quantity' => 'required'
            ],
            [
                'required' => 'O campo deve ser preenchido!',
                'min' => 'O campo deve conter no mínimo 3 caracteres!',
                'max' => 'Limite de caracteres atingido!'
            ]
        );

        //$solicitation = new ClientSolicitation();
        //$solicitation->fill($request->all());

        // if ($solicitation->save()) {
        //     return redirect()->route('solicitations.index');
        // }

        $solicitation = Solicitation::find($request->get('solicitation_id'));

        $solicitation->products()->attach(
            $request->get('product_id'),
            [
                'quantity' => $request->get('quantity')
            ]
        );

        return redirect()->route('solicitations.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //METODO CONVENCIONAL
        // ClientSolicitation::where([
        //     'products_id' => $request->query('products_id'),
        //     'solicitation_id' => $request->query('solicitation_id')
        // ])->delete();

        // DELETE PELO RELACIONAMENTO
        // $solicitation = Solicitation::find($request->query('solicitation_id'));
        // $solicitation->products()->detach( $request->get('solicitation_id') );

        $cs = ClientSolicitation::find($id);

        if ($cs->delete()) {

            return redirect()->route('solicitations.index');

        }

    }
}
