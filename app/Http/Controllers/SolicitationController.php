<?php

namespace App\Http\Controllers;

use App\Client;
use App\Solicitation;
use Illuminate\Http\Request;

class SolicitationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if (!empty($request->all())) {

            $solicitationsList = Solicitation::with(['clients'])
                ->where('client_id', 'like', "%" . $request->input('name')."%")
                ->paginate(20);

        } else {
            $solicitationsList = Solicitation::with(['clients'])->paginate(20);
        }

        return view('app.solicitation.index', ['solicitationsList' => $solicitationsList, 'queryParams' => $request->all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $clientList = Client::all();

        return view('app.solicitation.create', ['clientList' => $clientList]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate(
            [
                'client_id' => 'exists:clients,id',
            ],
            [
                'required' => 'O campo deve ser preenchido!',
                'min' => 'O campo deve conter no mínimo 3 caracteres!',
                'max' => 'Limite de caracteres atingido!'
            ]
        );

        $solicitation = new Solicitation();
        $solicitation->fill($request->all());

        if ($solicitation->save()) {
            return redirect()->route('solicitations.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $solicitation = Solicitation::find($id);

        $clientList = Client::all();

        return view('app.solicitation.show', ['solicitation' => $solicitation, 'clientList' => $clientList]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $solicitation = Solicitation::find($id);

        $clientList = Client::all();

        return view('app.solicitation.edit', ['solicitation' => $solicitation, 'clientList' => $clientList]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate(
            [
                'client_id' => 'exists:clients,id',
            ],
            [
                'required' => 'O campo deve ser preenchido!',
                'min' => 'O campo deve conter no mínimo 3 caracteres!',
                'max' => 'Limite de caracteres atingido!'
            ]
        );

        $solicitation = Solicitation::find($id);

        if ($solicitation->update($request->all())) {
            return redirect()->route('solicitations.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
