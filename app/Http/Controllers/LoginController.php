<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{


    public function index(Request $request)
    {
        $erro = !empty($request->get('error')) ? $request->get('error') : '';

        return view('site.login', ['error' => $erro]);
    }

    public function authentication(Request $request)
    {

        // Regras de validação
        $validationRules = [
            'user' => 'email',
            'password' => 'required'
        ];

        $feedback = [
            'user.email' => 'O usuário informado não é valido',
            'password.required' => 'O campo senha deve ser informado',
        ];

        $request->validate($validationRules, $feedback);

        $user = new User();

        $existe = $user->where('email', $request->get('user'))->where('password', $request->get('password'))->get()->first();

        if (isset($existe->name)) {
            session_start();

            $_SESSION['USER']['name'] = $existe->name;
            $_SESSION['USER']['email'] = $existe->email;

            return redirect()->route('app.home');
        }

        return redirect()->route('site.login', ['error' => 'Usuário ou senha inválidos.']);
    }

    public function logout(Request $request)
    {
        session_destroy();

        return redirect()->route('site.index');
    }
}
