<?php

namespace App\Http\Controllers;

use App\Measurement;
use App\Product;
use App\Supplier;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!empty($request->all())) {

            $productsList = Product::with(['measurement', 'supplier'])->where('name', 'like', "%" . $request->input('name')."%")
                    ->where('description', 'like', "%" . $request->input('description') . "%")
                    ->where('weight', 'like', "%" . $request->input('weight') . "%")
                    ->where('measurement_id', 'like', "%" . $request->input('measurement_id') . "%")
                    ->paginate(20);

        } else {
            $productsList = Product::with(['measurement', 'supplier'])->paginate(20);
        }

        return view('app.product.index', ['productsList' => $productsList, 'queryParams' => $request->all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $measurementList = Measurement::all();

        $suppliersList = Supplier::all();

        return view('app.product.create', ['measurementList' => $measurementList, 'suppliersList' => $suppliersList]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate(
            [
                'name' => 'required|min:3|max:150',
                'description' => 'required|min:3|max:2000',
                'weight' => 'required',
                'measurement_id' => 'exists:measurement,id',
                'supplier_id' => 'exists:supplier,id',
            ],
            [
                'required' => 'O campo deve ser preenchido!',
                'min' => 'O campo deve conter no mínimo 3 caracteres!',
                'max' => 'Limite de caracteres atingido!'
            ]
        );

        $product = new Product();
        $product->fill($request->all());

        if ($product->save()) {
            return redirect()->route('products.index');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);

        $measurementList = Measurement::all();

        $suppliersList = Supplier::all();

        return view('app.product.show', ['product' => $product, 'measurementList' => $measurementList, 'suppliersList' => $suppliersList]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $product = Product::find($id);

        $measurementList = Measurement::all();

        $suppliersList = Supplier::all();

        return view('app.product.edit', ['product' => $product, 'measurementList' => $measurementList, 'suppliersList' => $suppliersList]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate(
            [
                'name' => 'required|min:3|max:150',
                'description' => 'required|min:3|max:2000',
                'weight' => 'required',
                'measurement_id' => 'exists:measurement,id',
                'supplier_id' => 'exists:supplier,id',
            ],
            [
                'required' => 'O campo deve ser preenchido!',
                'min' => 'O campo deve conter no mínimo 3 caracteres!',
                'max' => 'Limite de caracteres atingido!'
            ]
        );

        $product = Product::find($id);

        if ($product->update($request->all())) {
            return redirect()->route('products.index');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $product = Product::find($id);

        if ($product->delete()) {

            return redirect()->route('products.index');

        }

    }
}
