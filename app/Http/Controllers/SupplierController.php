<?php

namespace App\Http\Controllers;

use App\Supplier;
use Illuminate\Http\Request;

use function Psy\debug;

class SupplierController extends Controller
{

    public function index(Request $request)
    {

        if (!empty($request->all())) {

            $suppliersList = Supplier::where('nome', 'like', "%" . $request->input('nome')."%")
                    ->where('site', 'like', "%" . $request->input('site') . "%")
                    ->where('email', 'like', "%" . $request->input('email') . "%")
                    ->where('uf', 'like', "%" . $request->input('uf') . "%")
                    ->where('cidade', 'like', "%" . $request->input('cidade') . "%")
                    ->paginate(20);

        } else {
            $suppliersList = Supplier::paginate(20);
        }

        return view('app.supplier.index', ['suppliersList' => $suppliersList, 'queryParams' => $request->all()]);
    }

    public function add()
    {

        return view('app.supplier.add');

    }

    public function save(Request $request)
    {

        $request->validate(
            [
                'nome' => 'required|min:3|max:150',
                'site' => 'required',
                'email' => 'email',
                'uf' => 'required',
                'cidade' => 'required|max:150'
            ],
            [
                'email.email' => 'O campo E-mail deve possuir um e-mail válido',
                'message.max' => 'O campo mensagem não pode ultrapassar 2000 caracteres!',

                'required' => 'O campo deve ser preenchido!',
            ]
        );

        if ($request->input('id') == null) {

            $supllier = new Supplier();
            $supllier->fill($request->all());

            if ($supllier->save()) {
                return redirect()->route('app.suppliers');
            }

        } else {

            $supllier = Supplier::find($request->input('id'));

            if ($supllier->update($request->all())) {
                return redirect()->route('app.suppliers');
            }

        }

    }

    public function edit($id)
    {

        $supplier = Supplier::find($id);

        return view('app.supplier.add', ['supplier' => $supplier]);

    }

    public function delete($id)
    {
        Supplier::find($id)->delete();

        return redirect()->route('app.suppliers');

    }

}
