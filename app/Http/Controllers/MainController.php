<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContactReason;

class MainController extends Controller
{

    public function index()
    {

        $contact_categories = ContactReason::all();

        return view('site.index', ["contact_categories" => $contact_categories]);
    }

}
