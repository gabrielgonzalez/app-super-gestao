<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solicitation extends Model
{

    //
    protected $table = 'solicitations';

    protected $fillable = ['client_id'];

    public function clients()
    {
        return $this->belongsTo('App\Client', 'client_id', 'id');
    }

    public function products()
    {
        return $this->belongsToMany('App\Product', 'client_solicitations')->withPivot(['id']);
    }

}
