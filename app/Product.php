<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = ['name', 'description', 'weight', 'measurement_id', 'supplier_id'];

    public function measurement()
    {
        return $this->belongsTo('App\Measurement');
    }

    public function supplier()
    {
        return $this->belongsTo('App\Supplier');
    }
}
