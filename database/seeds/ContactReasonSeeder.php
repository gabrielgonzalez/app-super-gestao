<?php

use Illuminate\Database\Seeder;
use App\ContactReason;

class ContactReasonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ContactReason::create(['description' => 'Dúvida', 'alias' => 'D']);
        ContactReason::create(['description' => 'Elogio', 'alias' => 'E']);
        ContactReason::create(['description' => 'Reclamação', 'alias' => 'R']);
    }
}
