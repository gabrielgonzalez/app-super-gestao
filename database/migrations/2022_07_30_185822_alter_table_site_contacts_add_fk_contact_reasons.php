<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AlterTableSiteContactsAddFkContactReasons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('site_contacts', function (Blueprint $table) {
            $table->unsignedBigInteger('contact_reasons_id');
        });

        DB::statement('update site_contacts set contact_reasons_id = contact_category');

        Schema::table('site_contacts', function (Blueprint $table) {
            $table->foreign('contact_reasons_id')->references('id')->on('contact_reasons');
            $table->dropColumn('contact_category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('site_contacts', function (Blueprint $table) {
            $table->integer('contact_category');
            $table->dropForeign('site_contacts_contact_reasons_id_foreign')->references('id')->on('contact_reasons');
        });

        DB::update('update site_contacts set contact_category = contact_reasons_id where contact_category IS NULL');

        Schema::table('site_contacts', function (Blueprint $table) {
            $table->dropColumn('contact_reasons_id');
        });
    }
}
