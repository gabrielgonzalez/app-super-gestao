<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use League\CommonMark\Extension\Table\Table;
use Symfony\Component\Console\Helper\Table as HelperTable;

class CreateTableMeasurement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('measurement', function (Blueprint $table) {
            $table->id();
            $table->string('unit', 150);
            $table->string('alias', 5);
            $table->timestamps();
        });

        Schema::table('products', function (Blueprint $table)
        {
            $table->unsignedBigInteger('measurement_id');
            $table->foreign('measurement_id')->references('id')->on('measurement');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('products', function (Blueprint $table)
        {
            $table->dropForeign('products_measurement_id_foreign');
            $table->dropColumn('measurement_id');
        });

        Schema::dropIfExists('measurement');
    }
}
