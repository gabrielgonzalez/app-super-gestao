<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientSolicitationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_solicitations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('solicitation_id');
            $table->unsignedBigInteger('product_id');
            $table->timestamps();

            $table->foreign('solicitation_id')->references('id')->on('solicitations');
            $table->foreign('product_id')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_solicitations');
    }
}
