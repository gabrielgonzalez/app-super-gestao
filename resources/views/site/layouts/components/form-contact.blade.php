<form action={{ route('site.contact.send') }} method="POST">
    @csrf
    <input name="name" value="{{ old('name') }}" type="text" placeholder="Nome" class="{{ $class }}">
    @component('site.layouts.errors', ["inputName" => "name"]) @endcomponent
    <br>
    <input name="phone" value="{{ old('phone') }}" type="text" placeholder="Telefone" class="{{ $class }}">
    @component('site.layouts.errors', ["inputName" => "phone"]) @endcomponent
    <br>
    <input name="email" value="{{ old('email') }}" type="text" placeholder="E-mail" class="{{ $class }}">
    @component('site.layouts.errors', ["inputName" => "email"]) @endcomponent
    <br>
    <select name="contact_reasons_id" class="{{ $class }}">
        <option value="">Qual o motivo do contato?</option>
        @foreach ($contact_categories as $key => $contact_category)
            <option value="{{$contact_category->id}}" {{ old('contact_reasons_id') == $contact_category->id ? 'selected' : '' }}>{{$contact_category->description}}</option>
        @endforeach
    </select>
    @component('site.layouts.errors', ["inputName" => "contact_reasons_id"]) @endcomponent
    <br>
    <textarea name="message" class="{{ $class }}" placeholder="Preencha aqui a sua mensagem">{{ old('message') }}</textarea>
    @component('site.layouts.errors', ["inputName" => "message"]) @endcomponent
    <br>
    <button type="submit" class="{{ $class }}">ENVIAR</button>
</form>
