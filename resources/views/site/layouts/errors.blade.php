@if ($errors->has($inputName))

    <div class="error-text">{{ $errors->first($inputName) }}</div>

@endif
