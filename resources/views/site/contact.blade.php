
@extends('site.layouts.default')

@section('title', 'Contato')

@section('content-body')

    <div class="titulo-pagina">
        <h1>Entre em contato conosco</h1>
    </div>

    <div class="informacao-pagina">
        <div class="contato-principal">
            @component('site.layouts.components.form-contact', ["class" => "borda-preta", "contact_categories" => $contact_categories]) @endcomponent
        </div>
    </div>

@endsection
