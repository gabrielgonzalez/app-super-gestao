
@extends('site.layouts.default')

@section('title', 'Login')

@section('content-body')

    <div class="titulo-pagina">
        <h1>Entre em contato conosco</h1>
    </div>

    <div class="informacao-pagina">

        <div style="width: 30%; margin-left: auto; margin-right: auto;">

            <form action="{{route('site.auth')}}" method="post">
                @csrf

                <input type="text" name="user" placeholder="Usuário" class="borda-preta" value="{{ old('user') }}">
                @component('site.layouts.errors', ["inputName" => "user"]) @endcomponent

                <input type="password" name="password" class="borda-preta" placeholder="Senha" value="{{ old('password') }}">
                @component('site.layouts.errors', ["inputName" => "password"]) @endcomponent

                <button type="submit" class="borda-preta">Acessar</button>
            </form>

            {{ $error }}

        </div>

    </div>

@endsection
