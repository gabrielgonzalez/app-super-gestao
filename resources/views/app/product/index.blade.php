@extends('app.layouts.default')

@section('title', 'Produtos')

@section('content-body')
    <div class="page-title-small">
        <h1>Produtos</h1>
    </div>

    <div class="menu">
        <ul>
            <li><a href="{{ route('products.create') }}">Novo</a></li>
        </ul>
    </div>

    <div class="informacao-pagina">

        <div style="width: 60%; margin-left: auto; margin-right: auto; ">

            <form action="{{ route('products.index') }}" method="get">
                @csrf
                <input type="text" name="name" placeholder="Nome">
                <input type="text" name="weight" placeholder="Peso">
                <input type="text" name="description" placeholder="Descrição">
                <input type="text" name="measurement_id" placeholder="Unidade">

                <button type="submit">Pesquisar</button>

            </form>

        </div>

    </div>

    <div class="informacao-pagina">

        <div style="width: 90%; margin-left: auto; margin-right: auto; ">

            <table border="1" width="100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nome</th>
                        <th>Fornecedor</th>
                        <th>Descrição</th>
                        <th>Peso</th>
                        <th>UM</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($productsList as $product)

                        <tr>
                            <td>
                                {{ $product->id }}
                            </td>
                            <td>
                                {{ $product->name }}
                            </td>
                            <td>
                                {{ $product->supplier->nome }}
                            </td>
                            <td>
                                {{ $product->description }}
                            </td>
                            <td>
                                {{ $product->weight }}
                            </td>
                            <td>
                                {{ $product->measurement->unit }} - {{ $product->measurement->alias }}
                            </td>
                            <td>
                                <a href="{{ route('products.show', $product->id) }}">Visualizar</a>
                                <a href="{{ route('products.edit', $product->id) }}">Editar</a>

                                <form action="{{ route('products.destroy', $product->id) }}" method="post">
                                    @csrf
                                    @method('DELETE')

                                    <button type="submit">Excluir</button>
                                </form>

                            </td>
                        </tr>

                    @endforeach
                </tbody>

                <footer>
                    <tr>
                        <td colspan="5"></td>
                        <td>
                            Total - {{ $productsList->total() }}
                        </td>
                    </tr>
                </footer>
            </table>

            <div>

                {{ $productsList->appends($queryParams)->links() }}

            </div>

        </div>

    </div>
@endsection
