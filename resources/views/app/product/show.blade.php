@extends('app.layouts.default')

@section('title', 'Produtos')

@section('content-body')
    <div class="page-title-small">
        <h1>Produtos - Editar</h1>
    </div>

    <div class="menu">
        <ul>
            <li><a href="{{ route('products.index') }}">Voltar</a></li>
        </ul>
    </div>

    <div class="informacao-pagina">

        <div style="width: 60%; margin-left: auto; margin-right: auto; ">

                <input disabled type="text" name="id" value="{{ $product->id ?? old('id') }}">

                <input disabled type="text" name="name" value="{{ $product->name ?? old('name') }}" placeholder="Nome">
                @component('site.layouts.errors', ["inputName" => "name"]) @endcomponent

                <input disabled type="text" name="description" value="{{ $product->description ?? old('description') }}" placeholder="Descrição">
                @component('site.layouts.errors', ["inputName" => "description"]) @endcomponent

                <input disabled type="number" name="weight" value="{{ $product->weight ?? old('weight') }}" placeholder="Peso">
                @component('site.layouts.errors', ["inputName" => "weight"]) @endcomponent

                <select name="measurement_id" disabled>
                    @foreach ($measurementList as $measurement)
                        <option value="{{$measurement->id}}" {{ (old('measurement_id') == $measurement->id || $product->measurement_id == $measurement->id) ? 'selected' : '' }} >{{ $measurement->unit }} - {{ $measurement->alias }}</option>
                    @endforeach
                </select>
                @component('site.layouts.errors', ["inputName" => "measurement_id"]) @endcomponent
        </div>

    </div>
@endsection
