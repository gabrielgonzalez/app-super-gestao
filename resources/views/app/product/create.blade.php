@extends('app.layouts.default')

@section('title', 'Produtos')

@section('content-body')
    <div class="page-title-small">
        <h1>Produtos - Adicionar</h1>
    </div>

    <div class="menu">
        <ul>
            <li><a href="{{ route('products.index') }}">Voltar</a></li>
        </ul>
    </div>

    <div class="informacao-pagina">

        <div style="width: 60%; margin-left: auto; margin-right: auto; ">

            <form action="{{ route('products.store') }}" method="post">
                @csrf

                <input type="hidden" name="id" value="{{ $product->id ?? old('id') }}">

                <input type="text" name="name" value="{{ $product->name ?? old('name') }}" placeholder="Nome">
                @component('site.layouts.errors', ["inputName" => "name"]) @endcomponent

                <input type="text" name="description" value="{{ $product->description ?? old('description') }}" placeholder="Descrição">
                @component('site.layouts.errors', ["inputName" => "description"]) @endcomponent

                <input type="number" name="weight" value="{{ $product->weight ?? old('weight') }}" placeholder="Peso">
                @component('site.layouts.errors', ["inputName" => "weight"]) @endcomponent

                <select name="measurement_id">
                    @foreach ($measurementList as $measurement)
                        <option value="{{$measurement->id}}" {{ old('measurement_id') == $measurement->id ? 'selected' : '' }} >{{ $measurement->unit }} - {{ $measurement->alias }}</option>
                    @endforeach
                </select>
                @component('site.layouts.errors', ["inputName" => "measurement_id"]) @endcomponent

                <select name="supplier_id">
                    @foreach ($suppliersList as $supplier)
                        <option value="{{$supplier->id}}" {{ old('supplier_id') == $supplier->id ? 'selected' : '' }} >{{ $supplier->nome }}</option>
                    @endforeach
                </select>
                @component('site.layouts.errors', ["inputName" => "supplier_id"]) @endcomponent

                <button type="submit">Salvar</button>

            </form>

        </div>

    </div>
@endsection
