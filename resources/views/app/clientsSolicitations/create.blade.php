@extends('app.layouts.default')

@section('title', 'Adicionar produto ao pedido')

@section('content-body')
    <div class="page-title-small">
        <h1>Adicionar produto ao pedido</h1>
    </div>

    <div class="menu">
        <ul>
            <li><a href="{{ route('solicitations.index') }}">Voltar</a></li>
        </ul>
    </div>

    <div class="informacao-pagina">

        <div style="width: 60%; margin-left: auto; margin-right: auto; ">
            <h3>Pedido do cliente: {{ $solicitation->clients->name }}</h3>

            <hr>

            <h4>Itens do pedido</h4>

            <table border="1" width="100%">
                <thead>
                    <tr>
                        <td><b>Produtos</b></td>
                        <td><b>Quantidade</b></td>
                        <td><b>Ações</b></td>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($solicitation->products as $products)
                        <tr>
                            <td>
                                {{ $products->name }}
                            </td>

                            <td>
                                {{ $products->quantity }}
                            </td>

                            <td>
                                <form id="form_{{$products->pivot->id}}" action="{{ route('clients-solicitations.destroy', $products->pivot->id) }}" method="post">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit">
                                        Remover
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            <form action="{{ route('clients-solicitations.store') }}" method="post">
                @csrf

                <input type="hidden"  name="solicitation_id" value="{{ $solicitation->id }}" >

                <select name="product_id">
                    <option value=""> -- Selecione um produto -- </option>
                    @foreach ($productList as $product)
                        <option value="{{$product->id}}" {{ (old('product_id') == $product->id) ? 'selected' : '' }} >{{ $product->name }}</option>
                    @endforeach
                </select>
                @component('site.layouts.errors', ["inputName" => "product_id"]) @endcomponent

                <input type="number" name="quantity" value="{{ old('quantity') ?? '' }}">
                @component('site.layouts.errors', ["inputName" => "quantity"]) @endcomponent

                <button type="submit">Salvar</button>

            </form>

        </div>

    </div>
@endsection
