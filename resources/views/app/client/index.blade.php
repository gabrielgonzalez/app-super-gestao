@extends('app.layouts.default')

@section('title', 'Clientes')

@section('content-body')
    <div class="page-title-small">
        <h1>Clientes</h1>
    </div>

    <div class="menu">
        <ul>
            <li><a href="{{ route('clients.create') }}">Novo</a></li>
        </ul>
    </div>

    <div class="informacao-pagina">

        <div style="width: 60%; margin-left: auto; margin-right: auto; ">

            <form action="{{ route('clients.index') }}" method="get">
                @csrf
                <input type="text" name="name" placeholder="Nome">
                <input type="text" name="email" placeholder="E-mail">
                <input type="text" name="cpfCnpj" placeholder="CPF/CNPJ">
                <input type="text" name="zipCode" placeholder="CEP">

                <button type="submit">Pesquisar</button>

            </form>

        </div>

    </div>

    <div class="informacao-pagina">

        <div style="width: 90%; margin-left: auto; margin-right: auto; ">

            <table border="1" width="100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nome</th>
                        <th>E-mail</th>
                        <th>CPF/CNPJ</th>
                        <th>CEP</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($clientsList as $client)

                        <tr>
                            <td>
                                {{ $client->id }}
                            </td>
                            <td>
                                {{ $client->name }}
                            </td>
                            <td>
                                {{ $client->email}}
                            </td>
                            <td>
                                {{ $client->cpfCnpj }}
                            </td>
                            <td>
                                {{ $client->zipCode }}
                            </td>
                            <td>
                                <a href="{{ route('clients.show', $client->id) }}">Visualizar</a>
                                <a href="{{ route('clients.edit', $client->id) }}">Editar</a>

                                <form action="{{ route('clients.destroy', $client->id) }}" method="post">
                                    @csrf
                                    @method('DELETE')

                                    <button type="submit">Excluir</button>
                                </form>

                            </td>
                        </tr>

                    @endforeach
                </tbody>

                <footer>
                    <tr>
                        <td colspan="5"></td>
                        <td>
                            Total - {{ $clientsList->total() }}
                        </td>
                    </tr>
                </footer>
            </table>

            <div>

                {{ $clientsList->appends($queryParams)->links() }}

            </div>

        </div>

    </div>
@endsection
