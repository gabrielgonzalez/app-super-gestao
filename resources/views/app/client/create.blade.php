@extends('app.layouts.default')

@section('title', 'Clientes')

@section('content-body')
    <div class="page-title-small">
        <h1>Clientes - Adicionar</h1>
    </div>

    <div class="menu">
        <ul>
            <li><a href="{{ route('clients.index') }}">Voltar</a></li>
        </ul>
    </div>

    <div class="informacao-pagina">

        <div style="width: 60%; margin-left: auto; margin-right: auto; ">

            <form action="{{ route('clients.store') }}" method="post">
                @csrf

                <input type="hidden" name="id" value="{{ $client->id ?? old('id') }}">

                <input type="text" name="name" value="{{ $client->name ?? old('name') }}" placeholder="Nome">
                @component('site.layouts.errors', ["inputName" => "name"]) @endcomponent

                <input type="text" name="email" value="{{ $client->email ?? old('email') }}" placeholder="E-mail">
                @component('site.layouts.errors', ["inputName" => "email"]) @endcomponent

                <input type="number" name="cpfCnpj" value="{{ $client->cpfCnpj ?? old('cpfCnpj') }}" placeholder="CPF/CNPJ">
                @component('site.layouts.errors', ["inputName" => "cpfCnpj"]) @endcomponent

                <input type="number" name="zipCode" value="{{ $client->zipCode ?? old('zipCode') }}" placeholder="CEP">
                @component('site.layouts.errors', ["inputName" => "zipCode"]) @endcomponent

                <button type="submit">Salvar</button>

            </form>

        </div>

    </div>
@endsection
