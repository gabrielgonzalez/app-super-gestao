@extends('app.layouts.default')

@section('title', 'Pedidos')

@section('content-body')
    <div class="page-title-small">
        <h1>Pedidos - Editar</h1>
    </div>

    <div class="menu">
        <ul>
            <li><a href="{{ route('solicitations.index') }}">Voltar</a></li>
        </ul>
    </div>

    <div class="informacao-pagina">

        <div style="width: 60%; margin-left: auto; margin-right: auto; ">

            <form action="{{ route('solicitations.update', [$solicitation->id]) }}" method="post">
                @csrf
                @method('PUT')

                <select name="client_id">
                    @foreach ($clientList as $client)
                        <option value="{{$client->id}}" {{ (old('client_id') == $client->id || $solicitation->client_id == $client->id) ? 'selected' : '' }} >{{ $client->name }}</option>
                    @endforeach
                </select>
                @component('site.layouts.errors', ["inputName" => "client_id"]) @endcomponent

                <button type="submit">Salvar</button>

            </form>

        </div>

    </div>
@endsection
