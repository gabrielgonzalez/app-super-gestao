@extends('app.layouts.default')

@section('title', 'Pedidos')

@section('content-body')
    <div class="page-title-small">
        <h1>Pedidos</h1>
    </div>

    <div class="menu">
        <ul>
            <li><a href="{{ route('solicitations.create') }}">Novo</a></li>
        </ul>
    </div>

    <div class="informacao-pagina">

        <div style="width: 60%; margin-left: auto; margin-right: auto; ">

            <form action="{{ route('solicitations.index') }}" method="get">
                @csrf
                <input type="text" name="name" placeholder="Nome">
                <input type="text" name="email" placeholder="E-mail">
                <input type="text" name="cpfCnpj" placeholder="CPF/CNPJ">
                <input type="text" name="zipCode" placeholder="CEP">

                <button type="submit">Pesquisar</button>

            </form>

        </div>

    </div>

    <div class="informacao-pagina">

        <div style="width: 90%; margin-left: auto; margin-right: auto; ">

            <table border="1" width="100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Cliente</th>
                        <th>Produtos</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($solicitationsList as $solicitation)

                        <tr>
                            <td>
                                {{ $solicitation->id }}
                            </td>
                            <td>
                                {{ $solicitation->clients->name }}
                            </td>
                            <td>
                                <a href="{{ route('clients-solicitations.create', ['solicitation' => $solicitation->id]) }}">Adicionar Produto</a>
                            </td>
                            <td>
                                <a href="{{ route('solicitations.show', $solicitation->id) }}">Visualizar</a>
                                <a href="{{ route('solicitations.edit', $solicitation->id) }}">Editar</a>

                                <form action="{{ route('solicitations.destroy', $solicitation->id) }}" method="post">
                                    @csrf
                                    @method('DELETE')

                                    <button type="submit">Excluir</button>
                                </form>

                            </td>
                        </tr>

                    @endforeach
                </tbody>

                <footer>
                    <tr>
                        <td colspan="2"></td>
                        <td>
                            Total - {{ $solicitationsList->total() }}
                        </td>
                    </tr>
                </footer>
            </table>

            <div>

                {{ $solicitationsList->appends($queryParams)->links() }}

            </div>

        </div>

    </div>
@endsection
