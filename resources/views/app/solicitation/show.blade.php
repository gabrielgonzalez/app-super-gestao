@extends('app.layouts.default')

@section('title', 'Pedidos')

@section('content-body')
    <div class="page-title-small">
        <h1>Pedidos - Editar</h1>
    </div>

    <div class="menu">
        <ul>
            <li><a href="{{ route('solicitations.index') }}">Voltar</a></li>
        </ul>
    </div>

    <div class="informacao-pagina">

        <div style="width: 60%; margin-left: auto; margin-right: auto; ">

                <input disabled type="text" name="id" value="{{ $solicitation->id ?? old('id') }}">

                <select name="client_id" disabled>
                    @foreach ($clientList as $client)
                        <option value="{{$client->id}}" {{ ($solicitation->client_id == $client->id) ? 'selected' : '' }} >{{ $client->name }}</option>
                    @endforeach
                </select>

        </div>

    </div>
@endsection
