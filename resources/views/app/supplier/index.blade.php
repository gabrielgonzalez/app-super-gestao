@extends('app.layouts.default')

@section('title', 'Fornecedores')

@section('content-body')
    <div class="page-title-small">
        <h1>Fornecedores</h1>
    </div>

    <div class="menu">
        <ul>
            <li><a href="{{ route('app.suppliers.add') }}">Novo</a></li>
        </ul>
    </div>

    <div class="informacao-pagina">

        <div style="width: 60%; margin-left: auto; margin-right: auto; ">

            <form action="{{ route('app.suppliers') }}" method="get">
                @csrf
                <input type="text" name="nome" placeholder="Nome">
                <input type="text" name="site" placeholder="Site">
                <input type="email" name="email" placeholder="E-mail">
                <input type="text" name="uf" placeholder="UF">
                <input type="text" name="cidade" placeholder="cidade">

                <button type="submit">Pesquisar</button>

            </form>

        </div>

    </div>

    <div class="informacao-pagina">

        <div style="width: 90%; margin-left: auto; margin-right: auto; ">

            <table border="1" width="100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nome</th>
                        <th>Site</th>
                        <th>E-mail</th>
                        <th>UF</th>
                        <th>Cidade</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($suppliersList as $supplier)

                        <tr>
                            <td>
                                {{ $supplier->id }}
                            </td>
                            <td>
                                {{ $supplier->nome }}
                            </td>
                            <td>
                                {{ $supplier->site }}
                            </td>
                            <td>
                                {{ $supplier->email }}
                            </td>
                            <td>
                                {{ $supplier->uf }}
                            </td>
                            <td>
                                {{ $supplier->cidade }}
                            </td>
                            <td>
                                <a href="{{ route('app.suppliers.edit', $supplier->id) }}">Editar</a>
                                <a href="{{ route('app.suppliers.delete', $supplier->id) }}">Excluir</a>
                            </td>
                        </tr>

                    @endforeach
                </tbody>

                <footer>
                    <tr>
                        <td colspan="6"></td>
                        <td colspan="6">
                            Total - {{ $suppliersList->total() }}
                        </td>
                    </tr>
                </footer>
            </table>

            <div>

                {{ $suppliersList->appends($queryParams)->links() }}

            </div>

        </div>

    </div>
@endsection

<script>
    function showListProducts(supplier_id) {

    }
</script>
