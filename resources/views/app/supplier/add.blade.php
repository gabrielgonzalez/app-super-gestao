@extends('app.layouts.default')

@section('title', 'Fornecedores')

@section('content-body')
    <div class="page-title-small">
        <h1>Fornecedores - Adicionar</h1>
    </div>

    <div class="menu">
        <ul>
            <li><a href="{{ route('app.suppliers') }}">Voltar</a></li>
        </ul>
    </div>

    <div class="informacao-pagina">

        <div style="width: 60%; margin-left: auto; margin-right: auto; ">

            <form action="{{ route('app.suppliers.save') }}" method="post">
                @csrf

                <input type="hidden" name="id" value="{{ $supplier->id ?? old('id') }}">

                <input type="text" name="nome" value="{{ $supplier->nome ?? old('nome') }}" placeholder="Nome">
                @component('site.layouts.errors', ["inputName" => "nome"]) @endcomponent

                <input type="text" name="site" value="{{ $supplier->site ?? old('site') }}" placeholder="Site">
                @component('site.layouts.errors', ["inputName" => "site"]) @endcomponent

                <input type="email" name="email" value="{{ $supplier->email ?? old('email') }}" placeholder="E-mail">
                @component('site.layouts.errors', ["inputName" => "email"]) @endcomponent

                <input type="text" name="uf" value="{{ $supplier->uf ?? old('uf') }}" placeholder="UF">
                @component('site.layouts.errors', ["inputName" => "uf"]) @endcomponent

                <input type="text" name="cidade" value="{{ $supplier->cidade ?? old('cidade') }}" placeholder="cidade">
                @component('site.layouts.errors', ["inputName" => "cidade"]) @endcomponent

                <button type="submit">Salvar</button>

            </form>

        </div>

    </div>
@endsection
