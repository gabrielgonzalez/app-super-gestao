<?php

use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'MainController@index')->name('site.index');

Route::get('/about', 'AboutController@index')->name('site.about');

Route::get('/contact', 'ContactController@index')->name('site.contact');

Route::post('/contact/send', 'ContactController@sendMessage')->name('site.contact.send');

Route::get('/login', 'LoginController@index')->name('site.login');

Route::post('/login', 'LoginController@authentication')->name('site.auth');

Route::middleware('auth.basic')->prefix('/app')->group(function() {

    Route::get('/home', 'HomeController@index')->name('app.home');

    Route::get('/logout', 'LoginController@logout')->name('app.logout');

    // Route::get('/clients', 'ClientController@index')->name('app.clients');

    Route::get('/suppliers', 'SupplierController@index')->name('app.suppliers');
    Route::get('/suppliers/add', 'SupplierController@add')->name('app.suppliers.add');
    Route::post('/suppliers/save', 'SupplierController@save')->name('app.suppliers.save');
    Route::get('/suppliers/edit/{id}', 'SupplierController@edit')->name('app.suppliers.edit');
    Route::get('/suppliers/delete/{id}', 'SupplierController@delete')->name('app.suppliers.delete');

    Route::resource('products', 'ProductController');

    Route::resource('clients', 'ClientController');

    Route::resource('solicitations', 'SolicitationController');

    Route::resource('clients-solicitations', 'ClientSolicitationController');

});
